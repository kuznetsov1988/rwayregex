import pandas as pd
import json
import re

from easygui import fileopenbox, exceptionbox
from tqdm import tqdm

def get_by_val(struct, name, val):
    if type(val) == str:
        for obj in struct:
            if obj[name].strip() == val.strip():
                return obj
    # print([obj[name] for obj in struct])
    # print(f'"{val}"')
    return None

class ValuExcluder:
    def __init__(self, json_path):
        '''
        Инициализируем исключатель значений
        Аргументы:
        - json_path - Путь к JSON с инструкцией
        '''
        with open(json_path, 'r', encoding='utf-8') as inp:
            self.json = json.load(inp)
        
        self.check_json()
        
        ## Забираем из JSON'а названия для колонок
        ## с id, текстом, субъектом РФ и районом:
        self.id_col = self.json["col_map"]["id"]
        self.text_col = self.json["col_map"]["text"]
        self.subject_col = self.json["col_map"]["subject"]
        self.district_col = self.json["col_map"]["district"]
        self.eng_ru_map = {
        'a': 'а',
        'b': 'в',
        'c': 'с',
        'e': 'е',
        'k': 'к',
        'x': 'х',
        'h': 'н',
        't': 'т',
        'p': 'р',
        'o': 'о',
        'm': 'м',
        'ё': 'е',
        'ё': 'е',
        'y': 'у'
    }
    
    def check_json(self):
        '''
        Проверяет, что в переданной конструктору
        JSON-инструкции правильно заполнены все нужные поля
        '''
        err_msgs = []

        if 'preprocess' in self.json:
            if 'replace' in self.json['preprocess']:
                if 'regex_replace' in self.json['preprocess']['replace']:
                    repls = self.json['preprocess']['replace']['regex_replace']
                    for repl in repls:
                        if "old" in repl:
                            if repl["old"]:
                                for reg in repl["old"]:
                                    try:
                                        re.compile(reg)
                                    except:
                                        err_msgs.append(f"Ошибка в регулярном выражении - {reg}")
                            else:
                                err_msgs.append('Пустой ключ "old" в "regex_replace"!')
                        else:
                            err_msgs.append('Отсутствует один из ключей "old" для "regex_replace"!')

        if 'col_map' in self.json:
            if not "subject" in self.json['col_map']:
                err_msgs.append('Отсутствует ключ "col_map" -> "subject"!')
            if not "district" in self.json['col_map']:
                err_msgs.append('Отсутствует ключ "col_map" -> "district"!')
            if not "id" in self.json['col_map']:
                err_msgs.append('Отсутствует ключ "col_map" -> "id"!')
            if not "text" in self.json['col_map']:
                err_msgs.append('Отсутствует ключ "col_map" -> "text"!')
        else:
            err_msgs.append('Отсутствует ключ "col_map"!')
        if 'subjects' in self.json:
            for subj in self.json['subjects']:
                for reg in subj["regex_neg"]:
                    try:
                        re.compile(reg)
                    except:
                        err_msgs.append(f"Ошибка в регулярном выражении - { reg }")
                for distr in subj["districts"]:
                    for reg in distr["regex_neg"]:
                        try:
                            re.compile(reg)
                        except:
                            err_msgs.append(f"Ошибка в регулярном выражении - { reg }")
                    for reg in distr["regex_pos"]:
                        try:
                            re.compile(reg)
                        except:
                            err_msgs.append(f"Ошибка в регулярном выражении - { reg }")
        else:
            err_msgs.append('Отсутствует ключ "subjects"!')
        
        if err_msgs:
            exceptionbox(msg='\n'.join(err_msgs), title="Ошибка")
    
    def check_subject_and_district(self, row):
        '''
        Проверяем по описанию Субъект и Район объекта
        Аргументы:
         - row - строка pandas.DataFram'а
        '''
        right_subj, right_distr = True, True

        # subject_neg = []
        # subject_pos = []
        # district_neg = []
        
        subj = get_by_val(self.json["subjects"], "name", row[self.subject_col])
        if subj:
            if "regex_neg" in subj:
                for reg in subj["regex_neg"]:
                    if re.search(reg, row["preprocessed_text"]):
                        right_subj, right_distr = False, False
                        # subject_neg = reg
                        break
            if "regex_pos" in subj:
                for reg in subj["regex_pos"]:
                    if re.search(reg, row["preprocessed_text"]):
                        right_subj, right_distr = True, True
                        # subject_pos = reg
                        break
            neg_regs = []
            if right_subj:
                distr = get_by_val(subj['districts'], "name", row[self.district_col])
                if distr:
                    if "regex_neg" in distr:
                        neg_regs = distr["regex_neg"]
                    else:
                        neg_regs = []
                    for distr1 in subj['districts']:
                        if 'regex_pos' in distr1:
                            if distr1['name'] != distr['name']:
                                neg_regs += distr1['regex_pos']
                    for reg in neg_regs:
                        if re.search(reg, row["preprocessed_text"]):
                            right_distr = False
                            # district_neg = reg
                            break
                # else:
                #     neg_regs = "аааа"
        return pd.Series([right_subj, right_distr])
        # return pd.Series([right_subj, right_distr, subject_neg, subject_pos, district_neg, neg_regs])

    def process_df(self, excel_path):
        '''
        Обрабатываем Excel-файл,
        удаляем из него объекты, для которых Субъект РФ и район указаны неправильно,
        сохраняем в новый Excel-файл с суфиксом '_filtered'
        Аргументы:
         - excel_path - Путь к Excel-файлу
        '''
        df = pd.read_excel(excel_path, index_col=self.id_col)
        excel_path = excel_path[:excel_path.rfind('.xlsx')]

        df['preprocessed_text'] = df[self.text_col]

        if "preprocess" in self.json:
            if "lower" in self.json["preprocess"]:
                if self.json["preprocess"]["lower"]:
                    df["preprocessed_text"] = df["preprocessed_text"].apply(lambda x: x.lower() if type(x)==str else "")
            if "trim" in self.json["preprocess"]:
                if self.json["preprocess"]["lower"]:
                    df["preprocessed_text"] = df["preprocessed_text"].apply(lambda x: re.sub('\s+', ' ', x))
            if "eng_rus" in self.json["preprocess"]:
                if self.json["preprocess"]["lower"]:
                    for i in self.eng_ru_map:
                        df["preprocessed_text"] = df["preprocessed_text"].apply(lambda x: x.replace(i, self.eng_ru_map[i]))
            
            if "replace" in self.json["preprocess"]:
                if "regex_replace" in self.json["preprocess"]["replace"]:
                    repls = self.json["preprocess"]["replace"]["regex_replace"]
                    for repl in repls:
                        # print(repl["old"])
                        for reg in repl["old"]:
                            df["preprocessed_text"] = df["preprocessed_text"].apply(lambda x: re.sub(reg, repl["new"], x))
        
        tqdm.pandas()
        df[['right_subject', 'right_district']] = df.progress_apply(self.check_subject_and_district, axis=1)
        # df[['right_subject', 'right_district',
        # 'subject_neg', 'subject_pos', 'district_neg', 'distr_neg_regs']] = df.progress_apply(self.check_subject_and_district, axis=1)
        # df = df[(df['right_subject'] == True) & (df['right_district'] == True)]
        df.to_excel(excel_path + '_filtered.xlsx')

def main():
    json_path = fileopenbox(msg="Выберите файл инструкции", title="Выбор инструкции", filetypes=["*.json"])
    excel_path = fileopenbox(msg="Выберите файл с данными", title="Выбор файла с данными", filetypes=["*.xlsx"])
    excluder = ValuExcluder(json_path)
    excluder.process_df(excel_path)

def test_value_excluder(json_path, data):
    excluder = ValuExcluder(json_path)

if __name__ == '__main__':
    main()