from flask import Flask, jsonify, request
from modules.data_extractor import DataExtractor

from config import api

app = Flask(__name__)


@app.route('/', methods=['GET'])
def about():
    jsn = {'app': 'Test'}
    return jsonify(jsn)


@app.route('/data_extractor/', methods=['POST'])
def get_value():
    data_extr = DataExtractor(request.json)
    data_extr.check_json()
    if len(data_extr.error):
        return jsonify(data_extr.error)
    data_extr.create_df()
    if "split_text" in data_extr.json:
        data_extr.split_text()
    if "classify_text" in data_extr.json:
        data_extr.classify_text(api_params=api)
    data_extr.regex_val()
    data_extr.get_res_json()

    ##print(data_extr.df.text[0])

    return jsonify(data_extr.res_json)


if __name__ == '__main__':
    app.run(host='localhost', port=7001, debug=False)
