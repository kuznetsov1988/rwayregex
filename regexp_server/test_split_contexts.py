from modules.data_extractor1 import split_contexts

import pandas as pd
import json

example_json = {'nltk': True,
 'split_local': [';'],
 'filter': {'regex_filter': ['[0-9]+( ?[.,] ?[0-9]+)? (квм|сотки|гектар)'],
  'cut_text': {'left_from_match': 5, 'right_from_match': 0}}}

example_text = "Продам дом 25 квм. Вместе с домом участок 3 сотки, баня 16 квм; Собственник"

print(split_contexts(example_text, example_json))

data = pd.read_excel("C:\\Users\\v.dorofeev\\Documents\\rwayregex\\not_for_pushing\\площадь_участков_тексты.xlsx")
data['contexts'] = data['text'].apply(lambda x: split_contexts(x, example_json))

data.to_excel("output.xlsx")