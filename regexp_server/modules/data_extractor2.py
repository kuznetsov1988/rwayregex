import pandas as pd
import re
from modules.get_stand_repls import *

# from pymorphy2.tokenizers import simple_word_tokenize
# from enchant import Dict

from numpy import isnan

# def spell_check(text, checker):
#     outp = ''
#     text = simple_word_tokenize(text)
#     for idx, word in enumerate(text):
#         if checker.check(word):
#             if idx and word not in (";!.,"):
#                 outp += ' '
#             outp += word
#         else:
#             suggestions = checker.suggest(word)
#             if suggestions:
#                 sugg = suggestions[0]
#                 if idx and sugg not in (";!.,"):
#                     outp += ' '
#                 outp += sugg
#     ##print(outp)
#     return outp

class DataExtractor():
    def __init__(self, json):
        self.error = {}
        self.df = {}
        self.res_json = []
        self.json = json
        if 'modf_text' in self.json:
            if 'spell_check' in self.json:
                if self.json['modf_text']['spell_check']:
                    ## requires RU dict and aff files in lib/enchant:
                    self.checker = Dict("ru_RU")

    def check_json(self):
        '''Проверяет Json на отсуствующие ключи'''
        if 'task' not in self.json:
            self.error = {"error": "wrong json",
                          'detail': 'json without key "task"'}
        if 'type' not in self.json:
            self.error = {"error": "wrong json",
                          'detail': 'json without key "type"'}
        if 'regex' not in self.json:
            self.error = {"error": "wrong json",
                          'detail': 'json without key "regex"'}
        if 'data' not in self.json:
            self.error = {"error": "wrong json",
                          'detail': 'json without key "data"'}

    def create_df(self):
        '''Создает датафрейм и изменяет текст'''
        def make_repl(row, repls):
            for key, val in repls.items():
                row = str(row)
                row = row.replace(key, val)
            row = row.replace(chr(776), '')
            return row

        self.df = pd.DataFrame(data=self.json['data'])
        self.df['value'] = None
        if 'modf_text' in self.json:
            if 'low_case' in self.json['modf_text']:
                if self.json['modf_text']['low_case']:
                    self.df['text'] = self.df['text'].str.lower()
            if 'replace' in self.json['modf_text']:
                repls = self.json['modf_text']['replace']
                if 'standart' in repls:
                    repl_stand = repls['standart']
                    if 'eng_rus' in repl_stand:
                        self.df['text'] = self.df['text'].\
                                          apply(lambda x: \
                                                make_repl(x, repl_eng_rus()))
                    if 'str_int_to_int' in repl_stand:
                        self.df['text'] = self.df['text'].\
                                          apply(lambda x: \
                                                make_repl(x, repl_str_int()))
                    if 'trim' in repl_stand:
                        self.df['text'] = self.df['text'].\
                                          apply(lambda x: \
                                                re.sub('\\s+', ' ', x))

                if 'regex_replace' in repls:
                    for repl in repls['regex_replace']:
                        regs = repl['old']
                        for reg in regs:
                            ## temporary:
                            # for text in self.df['text']:
                            #     match = re.search(reg, text)
                            #     if match:
                            #         #print(match.group(0))
                            #print(reg)
                            try:
                                re.compile(reg)
                            except re.error:
                                # pass
                                print(f"Bad regexp - {reg}")
                            self.df['text'] = self.df['text'].\
                                              apply(lambda x:\
                                                    re.sub(reg, repl['new'], x))
                if 'local' in repls:
                    for repl in repls['local']:
                        repl_old = repl['old']
                        for old in repl_old:
                            self.df['text'] = \
                            self.df['text'].str.replace(old, repl['new'])
            
            # # Printing every text in dataframe once:
            # for i in range(self.df.shape[0]):
            #     print(self.df.iloc[i]['text'])
            
            # if 'spell_check' in self.json['modf_text']:
            #     if self.json['modf_text']['spell_check']:
            #         #print("Spellchecking ... ")
            #         self.df['text'] = self.df['text'].apply(lambda x: spell_check(x, self.checker))

    def regex_val(self):
        '''Использовать регулярное выражение'''
        def reg(value, text, reg_flt, reg_fnd, reg_neg, multi, name):
            '''Если задана регулярка фильтрации использовать её сначала
               Используется для get_float_from_text'''
            if value is None or value == [] or value == [[], None]:
                if not reg_flt == '':
                    lst_x = re.findall(reg_flt, text)
                    x = []
                    for elm in lst_x:
                        el = re.findall(reg_fnd, elm)
                        for e in el:
                            if type(e) == tuple:
                                for et in e:
                                    et = float(et.replace(',', '.'))
                                    et = et * multi
                                    if et not in x:
                                        x.append(et)
                            else:
                                e = float(e.replace(',', '.')) * multi
                                if e not in x:
                                    x.append(e)
                else:
                    x = re.findall(reg_fnd, text)
                if x is not None and not x == []:
                    res = [x, reg_fnd, name]
                else:
                    res = [x, None]
                return res
            else:
                return value

        def reg_many_float(value, text, reg_flt, reg_fnd, reg_neg, multi, name):
            '''Если задана регулярка фильтрации использовать её сначала
               Используется для get_many_float_from_text'''
            if value is None or value == [] or value == [[], None]:
                value = []
            x = []
            lst_x = re.findall(reg_fnd, text)
            for elm in lst_x:
                elm = float(elm.replace(',', '.'))
                elm = elm * multi
                x.append(elm)

            if x is not None and not x == []:
                res = [x, reg_fnd, name]
                value.append(res)
            return value

        def reg_spr(value, text, reg_fnd, name, reg_neg):
            # print(text)
            try:
                re.compile(reg_fnd)
            except:
                print(f"Bad regexp - {reg_fnd}")
                raise Exception
            '''Используется для get_str_from_text'''
            if value is None or value == [] or value == [[], None]:
                x_lst = []
                reg_lst = []
            else:
                x_lst = value[0]
                reg_lst = value[1]
            x = re.search(reg_fnd, text)
            #if x:
                ##print(text, x)

            if not reg_neg == '':
                neg = re.findall(reg_neg, text)
                if len(neg):
                    x = None

            if x is not None:
                if name not in x_lst:
                    x_lst.append(name)
                    reg_lst.append(reg_fnd)

            res = [x_lst, reg_lst]

            return res

        def reg_naz(value, text, reg_flt, reg_fnd, name, group, podsegm, reg_neg):
            '''Используется для get_nazn_from_text'''
            if value is None:
                value = []

            if not reg_neg == '':
                neg = re.findall(reg_neg, text)
                if len(neg):
                    return value

            if not reg_flt == '':
                tpl_x = re.findall(reg_flt, text)
                for lst in tpl_x:
                    if not type(lst) == tuple:
                        lst = (lst,)
                    for elm in lst:
                        if len(elm):
                            fnd = re.findall(reg_fnd, elm)
                            if len(fnd):
                                res = {
                                    "name": name,
                                    "group": group,
                                    "podsegm": podsegm,
                                    "reg_fnd": reg_fnd
                                }
                                if res not in value:
                                    value.append(res)
            return value


        if not len(self.json['regex']):
            self.error = {"error": "wrong json",
                          'detail': 'empty regex list'}
            return
        if self.json['type'] == "get_float_from_text":
            for reg_dict in self.json['regex']:
                if 'regex_filter' not in reg_dict:
                    reg_dict['regex_filter'] = ''
                if 'regex_neg' not in reg_dict:
                    reg_dict['regex_neg'] = ''
                if 'multi' not in reg_dict:
                    reg_dict['multi'] = 1

                self.df['value'] = \
                self.df.apply(lambda x: reg(x.value,
                                            x.text,
                                            reg_dict['regex_filter'],
                                            reg_dict['regex_find'],
                                            reg_dict['regex_neg'],
                                            reg_dict['multi'],
                                            reg_dict['name']),
                                            axis=1)
        if self.json['type'] == "get_many_float_from_text":
            for reg_dict in self.json['regex']:
                if 'regex_filter' not in reg_dict:
                    reg_dict['regex_filter'] = ''
                if 'regex_neg' not in reg_dict:
                    reg_dict['regex_neg'] = ''
                if 'multi' not in reg_dict:
                    reg_dict['multi'] = 1
                self.df['value'] = \
                self.df.apply(lambda x: reg_many_float(x.value,
                                                       x.text,
                                                       reg_dict['regex_filter'],
                                                       reg_dict['regex_find'],
                                                       reg_dict['regex_neg'],
                                                       reg_dict['multi'],
                                                       reg_dict['name']),
                                                       axis=1)
        elif self.json['type'] == "get_str_from_text":
            only_shortest = False
            if "modf_text" in self.json:
                if "only_shortest_str" in self.json['modf_text']:
                    if self.json['modf_text']['only_shortest_str']:
                        only_shortest = True
            ## чтобы в датафрейм записывался наименьший по длине метч:
            if only_shortest:
                self.df['value'] =\
                self.df.apply(lambda x: self.apply_regex(x.text, x.value), axis=1)
                # if not "value" in self.df:
                #     self.df["value"] = [[] for i in range(self.df.shape[1])]
                # for index, row in self.df.iterrows():
                #     self.df.at[index, 'value'] = self.apply_regex(row['text'], row['value'])
            else:
                for reg_dict in self.json['regex']:
                    if 'regex_filter' not in reg_dict:
                        reg_dict['regex_filter'] = ''
                    if 'regex_neg' not in reg_dict:
                        reg_dict['regex_neg'] = ''
                    self.df['value'] = \
                    self.df.apply(lambda x: reg_spr(x.value,
                                                    x.text,
                                                    reg_dict['regex_find'],
                                                    reg_dict['name'],
                                                    reg_dict['regex_neg']),
                                                    axis=1)

        elif self.json['type'] == "get_nazn_from_text":
            for reg_dict in self.json['regex']:
                if 'regex_filter' not in reg_dict:
                    reg_dict['regex_filter'] = ''
                if 'regex_neg' not in reg_dict:
                    reg_dict['regex_neg'] = ''
                self.df['value'] = \
                self.df.apply(lambda x: reg_naz(x.value,
                                                x.text,
                                                reg_dict['regex_filter'],
                                                reg_dict['regex_find'],
                                                reg_dict['name'],
                                                reg_dict['group'],
                                                reg_dict['podsegm'],
                                                reg_dict['regex_neg']),
                                                axis=1)

    def apply_regex(self, text, value):
        '''Используется для get_str_from_text'''

        # print(text)
        if value is None or value == [] or value == [[], None]:
            x_lst = []
            reg_lst = []
        else:
            x_lst = value[0]
            reg_lst = value[1]
        
        spans1 = []
        spans2 = []

        for reg_dict in self.json["regex"]:
            # x = re.search(reg_dict["regex_find"], text)
            ## to get all matches and not just the first:
            x = list(re.finditer(reg_dict["regex_find"], text))
            #print([i for i in x], reg_dict["name"])
            no_neg = True
            if "regex_neg" in reg_dict:
                neg_matches = re.findall(reg_dict["regex_neg"], text)
                if len(neg_matches):
                    #print(reg_dict["name"])
                    #print("Neg matches: ")
                    #print(re.search(reg_dict["regex_neg"], text))
                    no_neg = False
                    #print("End of neg matches")
            if no_neg and x:
                #print("adding category - ", reg_dict["name"])
                if "only_unclassified" in reg_dict:
                    if reg_dict["only_unclassified"]:
                        spans2 += [(y, reg_dict["name"], reg_dict["regex_find"]) for y in x]
                        continue
                spans1 += [(y, reg_dict["name"], reg_dict["regex_find"]) for y in x]

        ## O(nlogn):  
        spans1 = sorted(spans1, key=lambda x: x[0].span())
        #print(spans1)

        prev_start_index = -1

        for i in range(len(spans1)):
            match, name, regex = spans1[i]
            span = match.span()
            #print(match, match.group(0))
            # to do:
            # также сделать с негативными регулярками - 
            # если есть негативная регулярка и позитивная с одинаковым индексом стартового символа,
            # то они сравниваются по длине
            # и срабатывает та, которая короче:
            if span[0] != prev_start_index:
                #print(prev_start_index, match, match.group(0))
                prev_start_index = span[0]
                if name not in x_lst:
                    x_lst.append(name)
                    reg_lst.append(regex)
        spans2 = sorted(spans2, key=lambda x: x[0].span())
        #print(spans1)

        # #print(spans1, spans2)

        prev_start_index = -1

        for i in range(len(spans2)):
            match, name, regex = spans2[i]
            span = match.span()
            #print(match, match.group(0))
            # to do:
            # также сделать с негативными регулярками - 
            # если есть негативная регулярка и позитивная с одинаковым индексом стартового символа,
            # то они сравниваются по длине
            # и срабатывает та, которая короче:
            if span[0] != prev_start_index:
                #print(prev_start_index, match, match.group(0))
                prev_start_index = span[0]
                if not x_lst:
                    x_lst.append(name)
                    reg_lst.append(regex)
                    break
        res = (x_lst, reg_lst)
        return res

    def get_res_json(self):
        '''Получить итоговый Json'''
        res_list = []
        for row in self.df.iterrows():
            dict_elem = row[1].to_dict()
            res_dict = {}
            res_dict['id'] = dict_elem['id']
            res_dict['text with replacements'] = dict_elem['text']

            for key in dict_elem:
                if key[0] == '_':
                    if type(dict_elem[key]) == float:
                        if not isnan(dict_elem[key]):
                            res_dict[key] = dict_elem[key]
                    else:
                        res_dict[key] = dict_elem[key]
            if self.json['type'] == "get_float_from_text":
                res_dict['value'] = []
                for x in dict_elem['value'][0]:
                    try:
                        if 'diap' in self.json['regex'][0]:
                            low = self.json['regex'][0]['diap']['lower_value']
                            upp = self.json['regex'][0]['diap']['upper_value']
                            if x > low and x < upp:
                                res_dict['value'].append(x)
                        else:
                            res_dict['value'].append(x)
                    except Exception:
                        pass
                res_dict['regex'] = dict_elem['value'][1]
                try:
                    res_dict['name'] = dict_elem['value'][2]
                except Exception:
                    res_dict['name'] = ''
            elif self.json['type'] == "get_many_float_from_text":
                res_dict = []
                for x in dict_elem['value']:
                    res_dct = {}
                    res_dct['value'] = x[0]
                    res_dct['regex'] = x[1]
                    res_dct['name'] = x[2]
                    res_dict.append(res_dct)
            elif self.json['type'] == "get_str_from_text":
                res_dict['value'] = dict_elem['value'][0]
                res_dict['regex'] = dict_elem['value'][1]
            elif self.json['type'] == "get_nazn_from_text":
                res_dict['value'] = dict_elem['value']

            res_list.append(res_dict)
        self.res_json = res_list
