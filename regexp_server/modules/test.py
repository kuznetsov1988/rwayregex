from data_extractor1 import split_contexts
import json

example_json = {'nltk': True,
 'split_local': [';'],
 'filter': {'regex_filter': ['[0-9]+( ?[.,] ?[0-9]+)? (квм|сотки|гектар)'],
  'cut_text': {'left_from_match': 5, 'right_from_match': 0}}}

example_text = "Продам дом 25 квм. Вместе с домом участок 3 сотки, баня 16 квм; Собственник"

print(split_contexts(example_text, example_json))